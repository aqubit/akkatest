package com

import akka.actor.{ActorSystem, Inbox, Props}
import scala.concurrent.duration._

object Application {

  def main( args:Array[String] ):Unit = {

    val system = ActorSystem("helloakka")
    val greeter = system.actorOf(Props[Greeter], "greeter")
    val inbox = Inbox.create(system)

    greeter ! WhoToGreet("akka")

    inbox.send(greeter, Greet)
    val Greeting(message) = inbox.receive(5 seconds)
    println(s"Greeting: $message")

  }

}
